﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform playerTransform;
        [SerializeField] private float enemyShipSpeed = 5;
        [SerializeField] private Renderer playerRenderer;

        private float chasingThresholdDistance = 1.0f;

        private Renderer enermyRenderer;


        void Awake()
        {
            enermyRenderer = GetComponent<Renderer>();
        }

        void Update()
        {
            MoveToPlayer();
        }

        private void OnDrawGizmos()
        {
            CollisionDebug();
        }
        private void CollisionDebug()
        {
            if (enermyRenderer != null && playerRenderer != null)
            {
                if (IntersectAABB(enermyRenderer.bounds, playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else 
                {
                    Gizmos.color = Color.white;
                }


                Gizmos.DrawWireCube(enermyRenderer.bounds.center, 2*enermyRenderer.bounds.extents);
                Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
            }
        }

        private bool IntersectAABB(Bounds a, Bounds b)
        {
            return (a.min.x <= b.max.x && a.max.x >= b.min.x) && (a.min.y <= b.max.y && a.max.y >= b.min.y);
        }

        private void MoveToPlayer()
        {
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShipSpeed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
            }

        }
    }    
}

