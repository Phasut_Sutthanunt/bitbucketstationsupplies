﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public GameObject startInterface;
    int score;
    public Text scoreText;
    string String = "EnermyTag";

    private void Start()
    {
        score = 0;
        startInterface.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnermyTag"))
        {
            other.gameObject.SetActive(false);
        }
    }
    public void CalculateScore()
    {
        
    }
    
    // Start is called before the first frame update
    public void GameStartInterface()
    {
        startInterface.SetActive(false);
    }

    public void ScoreText()
    {
        scoreText.text = "Score : " + score;
    }

}
